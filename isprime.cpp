/**
 * isprime.cpp
 *
 * Ask the user for an integer and output whether or not the number
 * provided is a prime number.
 *
 * Created: 15.38pm Monday, 21 September 2020
 * Last updated Time-stamp: <15.52pm Monday, 21 September 2020>
 *
 * Author: Barry D. Nichols <B.Nichols@mdx.ac.uk>
 **/
#include <iostream>

using namespace std;

int main(){

  int number;
  cout << "Enter an integer: ";
  cin >> number;

  // test divisors of number, if a divisor other than 1 and number is
  // found, then number is not prime.

  bool isPrime = true;

  for (int i = 2; i <= number/2; ++i)
    if (number % i == 0)
      isPrime = false;

  if (!isPrime)
   	cout << "Not Prime\n";
  else
        cout << "prime\n";
  
  return 0;
}
